# Big Number Processor

### Setup
Install Java (version 8+) if not already present (tested against Java 8)

Run `./gradlew build`

This generates an executable jar at `build/libs/big-number-processor.jar`.

Caveat:  There are some janky tests that with `Thread.sleeps` in them to test for race conditions that work on my machine but may not work on yours.
To bypass tests, run `./gradlew build -x check`

You can view usage instructions by running the jar with no arguments.


```
 $ ./build/libs/big-number-processor.jar
No command found
Usage: <main class> [command] [command options]
  Commands:
    generate      generate new line delimited big integers
      Usage: generate [options]
        Options:
        * --count
            number of random numbers to generate
            Default: 0

    process      process a bunch of big numbers
      Usage: process [options]
        Options:
          --file
            File to read numbers from. If no file is provided random numbers
            will be used
          --threshold
            threshold to count numbers higher than (default 0)
            Default: 0

```

### Sample data generation

There is an included utility to generate sample data:

`build/libs/big-number-processor.jar generate --count <count>`

Example:

`build/libs/big-number-processor.jar generate --count 1000000 > sample.txt`

### Processing data

To process a data set run the `process` command and pass in a file and threshold number.

`build/libs/big-number-processor.jar process --file <file> --threshold <threshold>`

If the threshold is omitted, it will default to `0`.

If the file is omitted, the application will run against a randomly generated infinite stream.

You may need to pass in the absolute path to the file.

### Stats
Stats (min/max/threshold) are available through the console (when the Queue/Flux closes), or through a set of http endpoints.

`/stats/min`, `/stats/max`, and `/stats/threshold` return the plain text values for each respective statistic (on port `8080` by default).

The `/stats/min` and `/stats/max` endpoints may return a blank response if no values have been processed.

### Design
This project is built off of [Project Reactor](https://projectreactor.io/), a library based off the [reactive streams initiative](http://www.reactive-streams.org/).
Reactor provides tools for easily building non-blocking applications, and is the technology that powers the [Spring WebFlux](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html).

The `Queue` abstraction for this application is not a traditional Queue/Deque datastructure, but instead a [Flux<String>](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Flux.html), which represents a (potentially infinite) stream of Strings.
Included are utilities to read a file into a `Flux<String>` or randomly generate one.
There are also reactor adapters for [Kafka](https://github.com/reactor/reactor-kafka) and [several other data stores](https://spring.io/blog/2016/11/28/going-reactive-with-spring-data), making it compatible with many use cases.

Reactor manages the multithreading aspect of the application by distributing work across threads with the [parallel scheduler](https://projectreactor.io/docs/core/release/reference/#advanced-parallelizing-parralelflux).
Each number is distributed to one of the parallel threads, checked for validity (a message is logged for each blank or otherwise invalid input value), then processed against a global data store.

The data store is a thread safe object backed by `AtomicReference`s for the min and max, and a `LongAdder` for the threshold count.
The threshold count is fairly straightforward, while the min/max checks perform a non-atomic/non-locking check to see if they are potentially a new min/max on their thread, and if that initial check passes then an atomic compare/update is performed.
This potentially leads to a bit of contention early on for the first couple iterations as the mins/maxes are updated more often, but greatly reduces contention once larger values are established.

### Performance

Using a sample file with 1 million records on a mid 2014 macbook pro:

`./build/libs/big-number-processor.jar generate --count 1000000 > sample1m.txt`

Single threaded (manually comment out the parallel scheduling lines):

```
./build/libs/big-number-processor.jar process --file $(pwd)/sample1m.txt

...omitted...

2018-12-09 22:32:16.754  INFO 53383 --- [           main] c.g.f.b.NumberProcessor                  : rate: [83333 ops/sec] threshold: [499016] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]

```

Parallel:

```
./build/libs/big-number-processor.jar process --file $(pwd)/sample1m.txt

...omitted...

2018-12-09 22:34:52.253  INFO 53516 --- [     parallel-2] c.g.f.b.NumberProcessor                  : rate: [333250 ops/sec] threshold: [498884] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.254  INFO 53516 --- [     parallel-8] c.g.f.b.NumberProcessor                  : rate: [333250 ops/sec] threshold: [498884] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.255  INFO 53516 --- [     parallel-6] c.g.f.b.NumberProcessor                  : rate: [333255 ops/sec] threshold: [498889] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.255  INFO 53516 --- [     parallel-7] c.g.f.b.NumberProcessor                  : rate: [333250 ops/sec] threshold: [498884] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.255  INFO 53516 --- [     parallel-5] c.g.f.b.NumberProcessor                  : rate: [333250 ops/sec] threshold: [498884] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.256  INFO 53516 --- [     parallel-3] c.g.f.b.NumberProcessor                  : rate: [333250 ops/sec] threshold: [498884] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.258  INFO 53516 --- [     parallel-4] c.g.f.b.NumberProcessor                  : rate: [333250 ops/sec] threshold: [498884] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]
2018-12-09 22:34:52.265  INFO 53516 --- [     parallel-1] c.g.f.b.NumberProcessor                  : rate: [250000 ops/sec] threshold: [499016] max: [some positive number with 1195 digits] min: [some negative number with 1196 digits]

```
(The multiple log lines are from the multiple threads terminating)


