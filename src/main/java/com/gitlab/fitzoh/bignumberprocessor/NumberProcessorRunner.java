package com.gitlab.fitzoh.bignumberprocessor;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.gitlab.fitzoh.bignumberprocessor.generate.RandomBigIntStringGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.io.File;
import java.nio.file.Files;

import static com.gitlab.fitzoh.bignumberprocessor.NumberProcessorRunner.COMMAND;

/**
 * Entrypoint for actually running the processor.
 */
@Component
@Profile(COMMAND)
public class NumberProcessorRunner implements CommandLineRunner {
    public static final String COMMAND = "process";
    public static final Params params = new NumberProcessorRunner.Params();
    private static final Logger log = LoggerFactory.getLogger(NumberProcessorRunner.class);
    private final NumberProcessor handler;
    private final RandomBigIntStringGenerator generator;

    public NumberProcessorRunner(NumberProcessor handler, RandomBigIntStringGenerator generator) {
        this.handler = handler;
        this.generator = generator;
    }

    @Override
    public void run(String... args) throws Exception {
        Flux<String> numbers;
        if (params.file == null) {
            log.info("no input file provided, using generated numbers");
            numbers = generator.flux();
        } else {
            numbers = Flux.fromStream(Files.lines(params.file.toPath()));
        }
        handler.process(numbers);
    }

    @Parameters(commandDescription = "process a bunch of big numbers")
    static class Params {
        @Parameter(names = "--file", description = "File to read numbers from. If no file is provided random numbers will be used")
        private File file;

        @Parameter(names = "--threshold", description = "threshold to count numbers higher than (default 0)")
        private String threshold = "0";

        public File getFile() {
            return file;
        }

        public String getThreshold() {
            return threshold;
        }
    }
}
