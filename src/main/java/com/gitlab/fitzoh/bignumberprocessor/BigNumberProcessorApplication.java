package com.gitlab.fitzoh.bignumberprocessor;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.gitlab.fitzoh.bignumberprocessor.generate.GenerateCommandRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class BigNumberProcessorApplication {

    /**
     * parses args and activates profiles to run either the {@link GenerateCommandRunner} or the {@link NumberProcessorRunner}
     */
    public static void main(String[] args) {

        JCommander jc = JCommander.newBuilder()
                .addCommand(GenerateCommandRunner.COMMAND, GenerateCommandRunner.params)
                .addCommand(NumberProcessorRunner.COMMAND, NumberProcessorRunner.params)
                .build();
        try {
            jc.parse(args);
        } catch (ParameterException e) {
            System.out.println(e.getMessage());
            jc.usage();
            System.exit(1);
        }
        String profile = jc.getParsedCommand();
        if (profile == null) {
            System.out.println("No command found");
            jc.usage();
            System.exit(1);
        }
        SpringApplication application = new SpringApplication(BigNumberProcessorApplication.class);
        application.setAdditionalProfiles(profile);
        application.setDefaultProperties(Collections.singletonMap("processor.threshold", NumberProcessorRunner.params.getThreshold()));
        application.run(args);
    }

}
