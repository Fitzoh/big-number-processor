package com.gitlab.fitzoh.bignumberprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.UnaryOperator;

/**
 * Stores current values for max value, min value, and count of numbers > the threshold.
 * This class is thread safe (max/min are {@link AtomicReference<BigInteger>}, and the threshold counter is a {@link LongAdder}.
 * <p>
 * To improve performance, non-atomic checks are made for min/max checks before attempting to update the atomic references.
 * <p>
 * There is a chance that the threshold count will be slightly due to the non-atomic nature of {@link LongAdder#sum()}
 */
@Component
public class NumberProcessorState {

    private static final Logger log = LoggerFactory.getLogger(NumberProcessorState.class);
    private final BigInteger threshold;
    private AtomicReference<BigInteger> max = new AtomicReference<>();
    private AtomicReference<BigInteger> min = new AtomicReference<>();
    private AtomicReference<Instant> firstRecord = new AtomicReference<>();
    private LongAdder thresholdCount = new LongAdder();
    private LongAdder numProcessed = new LongAdder();

    public NumberProcessorState(@Value("${processor.threshold:0}") @NonNull String threshold) {
        this.threshold = new BigInteger(threshold);
        log.info("state initialize with threshold {}", this.threshold);
    }

    /**
     * Entry point for processing a new {@link BigInteger}.
     */
    public void handleNextNumber(@NonNull BigInteger number) {
        if (firstRecord.get() == null) {
            firstRecord.compareAndSet(null, Instant.now());
        }
        checkMin(number);
        checkMax(number);
        checkThreshold(number);
        numProcessed.increment();
    }

    /**
     * Perform a non-atomic check of to see if {@param candidate} might be a new min.
     * If check succeeds, do atomic compare/update.
     */
    private void checkMin(BigInteger candidate) {
        BigInteger currentMin = min.get();
        if (currentMin == null || candidate.compareTo(currentMin) < 0) {
            min.updateAndGet(minUpdateFunction(candidate));
        }
    }

    /**
     * @return an update function for use in {@link AtomicReference#updateAndGet(UnaryOperator)}
     */
    private UnaryOperator<BigInteger> minUpdateFunction(BigInteger candidate) {
        return currentMin -> {
            if (currentMin == null) {
                return candidate;
            }
            boolean larger = candidate.compareTo(currentMin) < 0;
            return larger ? candidate : currentMin;
        };
    }

    /**
     * Perform a non-atomic check of to see if {@param candidate} might be a new max.
     * If check succeeds, do atomic compare/update.
     */
    private void checkMax(BigInteger candidate) {
        BigInteger currentMax = max.get();
        if (currentMax == null || candidate.compareTo(currentMax) > 0) {
            max.updateAndGet(maxUpdateFunction(candidate));
        }
    }

    /**
     * @return an update function for use in {@link AtomicReference#updateAndGet(UnaryOperator)}
     */
    private UnaryOperator<BigInteger> maxUpdateFunction(BigInteger candidate) {
        return currentMax -> {
            if (currentMax == null) {
                return candidate;
            }
            boolean smaller = candidate.compareTo(currentMax) > 0;
            return smaller ? candidate : currentMax;
        };
    }


    private void checkThreshold(BigInteger number) {
        if (number.compareTo(threshold) > 0) {
            thresholdCount.increment();
        }
    }

    /**
     * @return the current (possibly null) minimum value
     */
    @Nullable
    public BigInteger getMin() {
        return min.get();
    }


    /**
     * @return the current (possibly null) maximum value
     */
    @Nullable
    public BigInteger getMax() {
        return max.get();
    }

    /**
     * @return a (possibly stale) threshold count.
     * See {@link LongAdder#sum()} documentation for details on atomicity
     */
    public long getThresholdCount() {
        return thresholdCount.sum();
    }

    /**
     * @return the (possibly stale) total count processed.
     * See {@link LongAdder#sum()} documentation for details on atomicity
     */
    public long getNumProcessed() {
        return numProcessed.sum();
    }

    /**
     * @return the time since the first record was processed
     */
    public Duration timeSinceFirstRecord() {
        if (firstRecord.get() == null) {
            return Duration.ZERO;
        }
        return Duration.between(firstRecord.get(), Instant.now());
    }

}
