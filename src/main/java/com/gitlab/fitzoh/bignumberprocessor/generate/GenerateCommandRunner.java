package com.gitlab.fitzoh.bignumberprocessor.generate;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import static com.gitlab.fitzoh.bignumberprocessor.generate.GenerateCommandRunner.COMMAND;


/**
 * Generate and print random integers to std out
 */
@Component
@Profile(COMMAND)
public class GenerateCommandRunner implements CommandLineRunner {


    public static final Params params = new Params();
    public static final String COMMAND = "generate";
    private final RandomBigIntStringGenerator generator;

    public GenerateCommandRunner(RandomBigIntStringGenerator generator) {
        this.generator = generator;
    }


    @Override
    public void run(String... args) {
        generator.flux()
                .take(params.getCount())
                .doOnEach(signal -> System.out.println(signal.get()))
                .blockLast();
    }

    @Parameters(commandDescription = "generate new line delimited big integers")
    static class Params {
        @Parameter(names = "--count", description = "number of random numbers to generate", required = true)
        private int count;

        public int getCount() {
            return count;
        }
    }
}
