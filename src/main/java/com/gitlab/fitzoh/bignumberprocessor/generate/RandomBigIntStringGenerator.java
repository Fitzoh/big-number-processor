package com.gitlab.fitzoh.bignumberprocessor.generate;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Utility class to generate some big integers to test with.
 */
@Component
public class RandomBigIntStringGenerator {


    /**
     * {@link Long#MAX_VALUE} has a max size of 2^63 - 1
     */
    public static final int MAX_LONG_NUM_BITS = 63;
    /**
     * Square the number of bits of {@link Long#MAX_VALUE} (we want *really* big numbers)
     */
    public static final int MAX_BITS = MAX_LONG_NUM_BITS * MAX_LONG_NUM_BITS;


    /**
     * Generate a random number string
     *
     * @return a String which parses to a valid positive or negative {@link BigInteger}, possibly larger/smaller than {@link Long#MAX_VALUE}/@{@link Long#MIN_VALUE}
     */
    public String next() {
        Random random = ThreadLocalRandom.current();
        BigInteger bigNum = new BigInteger(random.nextInt(MAX_BITS), random);
        return random.nextBoolean() ?
                bigNum.toString() :
                bigNum.negate().toString();
    }

    /**
     * @return a {@link Flux} of random numbers (in string form)
     */
    public Flux<String> flux() {
        //There might be a better way to do this, but I want to make sure the calls to `next()` happen on multiple threads
        BigInteger ignored = new BigInteger("0");
        return Flux.generate(sink -> sink.next(ignored))
                .parallel()
                .runOn(Schedulers.parallel())
                .map(i -> next())
                .sequential();
    }

}
