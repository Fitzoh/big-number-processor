package com.gitlab.fitzoh.bignumberprocessor;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.math.BigInteger;

/**
 *
 */
@Component
public class NumberProcessor {

    private static final Logger log = LoggerFactory.getLogger(NumberProcessor.class);

    private final NumberProcessorState processorState;

    public NumberProcessor(NumberProcessorState processorState) {
        this.processorState = processorState;
    }

    public void process(Flux<String> numbers) {
        Disposable subscription = numbers
                .parallel()
                .runOn(Schedulers.parallel())
                .flatMap(this::convertString)
                .doOnComplete(this::printSummary)
                .subscribe(processorState::handleNextNumber);


        Runtime.getRuntime().addShutdownHook(new Thread(subscription::dispose));
    }

    private Mono<BigInteger> convertString(String str) {
        if (Strings.isBlank(str)) {
            log.warn("blank input found, discarding");
            return Mono.empty();
        }
        try {
            BigInteger bigInt = new BigInteger(str);
            return Mono.just(bigInt);
        } catch (NumberFormatException e) {
            log.warn("discarding invalid input: {}", str);
            return Mono.empty();
        }
    }

    private void printSummary() {
        BigInteger max = processorState.getMax();
        BigInteger min = processorState.getMin();
        if (max == null || min == null) {
            log.warn("no items processed");
        }
        log.info("rate: [{}] threshold: [{}] max: [{}] min: [{}]",
                getRate(),
                processorState.getThresholdCount(),
                bigNumSmallString(max),
                bigNumSmallString(min));
    }

    private String bigNumSmallString(BigInteger integer) {
        if (integer == null) {
            return "null";
        }
        int numDigits = integer.toString().length();
        if (numDigits < 10) {
            return integer.toString();
        }
        if (integer.signum() == 1) {
            return "some positive number with " + numDigits + " digits";
        } else if (integer.signum() == -1) {
            return "some negative number with " + numDigits + " digits";
        }
        //signum should only return -1, 0, or 1, so we really shoudln't hit this, right?
        return "some number with " + numDigits + " digits";
    }

    private String getRate() {
        long millisSinceStart = processorState.timeSinceFirstRecord().toMillis();
        if (millisSinceStart == 0) {
            return "n/a";
        }
        long secondsSinceStart = millisSinceStart / 1000;
        if (secondsSinceStart == 0) {
            return "n/a";
        }
        long rate = processorState.getNumProcessed() / secondsSinceStart;
        return rate + " ops/sec";
    }
}
