package com.gitlab.fitzoh.bignumberprocessor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
@RequestMapping("/stats")
public class StatsController {

    private final NumberProcessorState state;

    public StatsController(NumberProcessorState state) {
        this.state = state;
    }

    @GetMapping("/min")
    private ResponseEntity<BigInteger> min() {
        return ResponseEntity.ok(state.getMin());
    }

    @GetMapping("/max")
    private ResponseEntity<BigInteger> max() {
        return ResponseEntity.ok(state.getMax());
    }

    @GetMapping("/threshold")
    private ResponseEntity<Long> threshold() {
        return ResponseEntity.ok(state.getThresholdCount());
    }

}
