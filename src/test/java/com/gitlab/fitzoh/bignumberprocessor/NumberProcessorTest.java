package com.gitlab.fitzoh.bignumberprocessor;


import com.gitlab.fitzoh.bignumberprocessor.generate.RandomBigIntStringGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.math.BigInteger;

import static org.assertj.core.api.Assertions.assertThat;

public class NumberProcessorTest {

    NumberProcessor numberProcessor;
    NumberProcessorState state;

    @BeforeEach
    void setUp() {
        state = new NumberProcessorState("0");
        numberProcessor = new NumberProcessor(state);
    }

    @Test
    @DisplayName("no values processed")
    void empty() {
        numberProcessor.process(Flux.empty());

        pause(100);

        assertThat(state.getMax()).isNull();
        assertThat(state.getMin()).isNull();
        assertThat(state.getThresholdCount()).isEqualTo(0);
        assertThat(state.getNumProcessed()).isEqualTo(0);
    }

    @Test
    @DisplayName("just a few values")
    void simple() {
        numberProcessor.process(Flux.just("-1", "0", "1"));

        pause(1000);

        assertThat(state.getMax()).isEqualTo(new BigInteger("1"));
        assertThat(state.getMin()).isEqualTo(new BigInteger("-1"));
        assertThat(state.getThresholdCount()).isEqualTo(1);
        assertThat(state.getNumProcessed()).isEqualTo(3);
    }

    @Test
    @DisplayName("check bad values")
    void badInput() {
        numberProcessor.process(Flux.just( "1", "", "dog"));

        pause(1000);

        assertThat(state.getMax()).isEqualTo(new BigInteger("1"));
        assertThat(state.getMin()).isEqualTo(new BigInteger("1"));
        assertThat(state.getThresholdCount()).isEqualTo(1);
        assertThat(state.getNumProcessed()).isEqualTo(1);
    }

    @Test
    @DisplayName("test large number of values")
    void bunchOfEm() {
        numberProcessor.process(
                new RandomBigIntStringGenerator().flux().take(1000)
        );
        pause(1000);


        assertThat(state.getMax()).isGreaterThan(new BigInteger("0"));
        assertThat(state.getMin()).isLessThan(new BigInteger("0"));
        assertThat(state.getThresholdCount()).isGreaterThan(0);
        assertThat(state.getNumProcessed()).isEqualTo(1000);

    }

    private void pause(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {

        }
    }
}