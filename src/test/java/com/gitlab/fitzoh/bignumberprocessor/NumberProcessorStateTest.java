package com.gitlab.fitzoh.bignumberprocessor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.time.Duration;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class NumberProcessorStateTest {

    NumberProcessorState state;
    BigInteger one = new BigInteger("1");
    BigInteger two = new BigInteger("2");
    BigInteger negOne = new BigInteger("-1");
    BigInteger negTwo = new BigInteger("-2");


    @BeforeEach
    void setUp() {
        state = new NumberProcessorState("0");
    }

    @Nested
    class Max {

        @Test
        @DisplayName("initial value is null")
        void initialNull() {
            assertThat(state.getMax()).isNull();
        }

        @Test
        @DisplayName("first value used as max")
        void initialValue() {
            state.handleNextNumber(one);
            assertThat(state.getMax()).isEqualTo(one);
        }

        @Test
        @DisplayName("larger number bumps max")
        void bump() {
            state.handleNextNumber(one);
            state.handleNextNumber(two);
            assertThat(state.getMax()).isEqualTo(two);
        }

        @Test
        @DisplayName("lower number not set to max")
        void ignoreLower() {
            state.handleNextNumber(two);
            state.handleNextNumber(one);
            assertThat(state.getMax()).isEqualTo(two);
        }

        @Test
        @DisplayName("play nice with negatives with smaller abs val")
        void ignoreSmallerNeg() {
            state.handleNextNumber(two);
            state.handleNextNumber(negOne);
            assertThat(state.getMax()).isEqualTo(two);
        }

        @Test
        @DisplayName("play nice with negatives with bigger abs val")
        void ignoreBiggerNeg() {
            state.handleNextNumber(one);
            state.handleNextNumber(negTwo);
            assertThat(state.getMax()).isEqualTo(one);
        }
    }

    @Nested
    class Min {


        @Test
        @DisplayName("initial value is null")
        void initialNull() {
            assertThat(state.getMin()).isNull();
        }

        @Test
        @DisplayName("first value used as max")
        void initialValue() {
            state.handleNextNumber(one);
            assertThat(state.getMin()).isEqualTo(one);
        }

        @Test
        @DisplayName("smaller number bumps min")
        void bump() {
            state.handleNextNumber(two);
            state.handleNextNumber(one);
            assertThat(state.getMin()).isEqualTo(one);
        }

        @Test
        @DisplayName("higher number not set to min")
        void ignoreLower() {
            state.handleNextNumber(one);
            state.handleNextNumber(two);
            assertThat(state.getMin()).isEqualTo(one);
        }

        @Test
        @DisplayName("play nice with negatives with smaller abs val")
        void ignoreSmallerNeg() {
            state.handleNextNumber(two);
            state.handleNextNumber(negOne);
            assertThat(state.getMin()).isEqualTo(negOne);
        }

        @Test
        @DisplayName("play nice with negatives with bigger abs val")
        void ignoreBiggerNeg() {
            state.handleNextNumber(one);
            state.handleNextNumber(negTwo);
            assertThat(state.getMin()).isEqualTo(negTwo);
        }
    }

    @Nested
    class Threshold {

        @Test
        @DisplayName("initial count is zero")
        void initialZero() {
            assertThat(state.getThresholdCount()).isEqualTo(0);
        }

        @Test
        @DisplayName("simple bump")
        void simpleBump() {
            state.handleNextNumber(one);
            assertThat(state.getThresholdCount()).isEqualTo(1);
        }

        @Test
        @DisplayName("multi-threaded bumps")
        void multiThreadBump() {
            IntStream.range(0, 1000).parallel()
                    .forEach(i -> state.handleNextNumber(one));
            assertThat(state.getThresholdCount()).isEqualTo(1000);
        }

        @Test
        @DisplayName("simple non-bump")
        void simpleNonBump() {
            state.handleNextNumber(negOne);
            assertThat(state.getThresholdCount()).isEqualTo(0);
        }

        @Test
        @DisplayName("multi-threaded non-bumps")
        void multiThreadNonBump() {
            IntStream.range(0, 1000).parallel()
                    .forEach(i -> state.handleNextNumber(negOne));
            assertThat(state.getThresholdCount()).isEqualTo(0);
        }

        @Test
        @DisplayName("simple bump and non-bump")
        void simpleMix() {
            state.handleNextNumber(one);
            state.handleNextNumber(negOne);
            assertThat(state.getThresholdCount()).isEqualTo(1);
        }

        @Test
        @DisplayName("multi-threaded bumps and non-bumps")
        void simpleMultiMix() {
            IntStream.range(0, 1000).parallel()
                    .forEach(i -> {
                        state.handleNextNumber(one);
                        state.handleNextNumber(negOne);
                    });
            assertThat(state.getThresholdCount()).isEqualTo(1000);
        }
    }

    @Nested
    class NumProcessed {

        @Test
        @DisplayName("initial value is zero")
        void initialZero() {
            assertThat(state.getNumProcessed()).isEqualTo(0);
        }

        @Test
        @DisplayName("it counts a single record")
        void single() {
            state.handleNextNumber(one);

            assertThat(state.getNumProcessed()).isEqualTo(1);
        }

        @Test
        @DisplayName("it counts a thousand records")
        void bunchOfEm() {
            IntStream.range(0, 1000).parallel()
                    .forEach(i -> state.handleNextNumber(one));
            assertThat(state.getNumProcessed()).isEqualTo(1000);
        }
    }

    @Nested
    class FirstRecord {

        @Test
        void initialNull() {
            assertThat(state.timeSinceFirstRecord()).isEqualTo(Duration.ZERO);
        }

        @Test
        void delay() throws Exception {
            state.handleNextNumber(one);
            Thread.sleep(10);
            assertThat(state.timeSinceFirstRecord()).isGreaterThanOrEqualTo(Duration.ofMillis(10));
        }
    }
}