package com.gitlab.fitzoh.bignumberprocessor;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigInteger;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = StatsController.class)
public class StatsControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    NumberProcessorState state;


    @Test
    void getMax() throws Exception {
        when(state.getMax()).thenReturn(new BigInteger("1"));
        mockMvc.perform(get("/stats/max")).andExpect(content().string("1"));
    }
    @Test
    void getMaxNull() throws Exception {
        when(state.getMax()).thenReturn(null);
        mockMvc.perform(get("/stats/max")).andExpect(content().string(""));
    }

    @Test
    void getMin() throws Exception {
        when(state.getMin()).thenReturn(new BigInteger("-1"));
        mockMvc.perform(get("/stats/min")).andExpect(content().string("-1"));
    }

    @Test
    void getMinNull() throws Exception {
        when(state.getMin()).thenReturn(null);
        mockMvc.perform(get("/stats/min")).andExpect(content().string(""));
    }


    @Test
    void getThresholdCount() throws Exception {
        when(state.getThresholdCount()).thenReturn(42l);
        mockMvc.perform(get("/stats/threshold")).andExpect(content().string("42"));
    }
}