package com.gitlab.fitzoh.bignumberprocessor.generate;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;

public class RandomBigIntStringGeneratorTest {

    RandomBigIntStringGenerator randomUtils = new RandomBigIntStringGenerator();
    Set<String> randoms = IntStream.range(0, 10_000)
            .mapToObj(i -> randomUtils.next())
            .collect(Collectors.toSet());

    @Test
    @DisplayName("it generates positive numbers")
    public void positive() {
        assertTrue(randoms.stream().anyMatch(s -> !s.startsWith("-")));
    }

    @Test
    @DisplayName("it generates negative numbers")
    public void negative() {
        assertTrue(randoms.stream().anyMatch(s -> s.startsWith("-")));
    }

    @Test
    @DisplayName("it generates really big numbers (100+ of digits)")
    public void large() {
        assertTrue(randoms.stream().anyMatch(s -> s.length() > 100));
    }

    @Test
    @DisplayName("it generates small numbers too")
    public void small() {
        assertTrue(randoms.stream().anyMatch(s -> s.length() < 100));
    }

    @Test
    @DisplayName("it generates valid big integers")
    public void valid() {
        randoms.stream().forEach(BigInteger::new);//no exception
    }
}